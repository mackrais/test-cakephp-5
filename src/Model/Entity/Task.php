<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <NameCategory>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackrRais
 *
 * @see       <https://mackrais.com>
 * @date      15.12.23
 */

declare(strict_types=1);

namespace App\Model\Entity;

use Authentication\PasswordHasher\DefaultPasswordHasher;

// Add this line
use Cake\ORM\Entity;

class Task extends Entity
{


    // Make all fields mass assignable except for primary key field "id".
    protected array $_accessible = [
        'name' => true,
        'created' => true,
        'modified' => true,
    ];

}
