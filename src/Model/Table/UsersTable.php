<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <NameCategory>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackrRais
 *
 * @see       <https://mackrais.com>
 * @date      15.12.23
 */

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public const ROLE_ADMIN = 'admin';
    public const ROLE_AUTHOR = 'author';

    public function initialize(array $config): void
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator): Validator
    {
        return $validator
            ->notBlank('email', 'An email is required')
            ->email('email')
            ->notBlank('password', 'A password is required')
            ->notBlank('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', [self::ROLE_AUTHOR, self::ROLE_ADMIN]],
                'message' => 'Please enter a valid role',
            ]);
    }

    // In a table class
    public function buildRules(RulesChecker|\Cake\ORM\RulesChecker $rules): \Cake\ORM\RulesChecker
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(
            ['email'],
            'This email has already been used.'
        ));

        return $rules;
    }

}
