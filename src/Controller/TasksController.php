<?php

/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <NameCategory>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackrRais
 *
 * @see       <https://mackrais.com>
 * @date      15.12.23
 */
declare(strict_types=1);

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\Http\Response;
use Cake\View\Exception\MissingTemplateException;

class TasksController extends AppController
{
    protected array $paginate = [
        'limit' => 5,
        'order' => [
            'Tasks.created' => 'desc',
        ],
    ];

    public function index()
    {
        $entity = $this->Tasks->newEmptyEntity();
        if ($this->request->is('post')) {
            $entity = $this->Tasks->patchEntity($entity, $this->request->getData());
            $entity->author_id = $this->Authentication->getIdentityData('id');
            if ($this->Tasks->save($entity)) {
                $this->Flash->success(__('The task has been saved.'));

                return $this->redirect(['action' => 'index', 'page' => 1]);
            }
            $this->Flash->error(__('Unable to add the task.'));
        }
        $this->set('task', $entity);
        $this->set('listTasks', $this->paginate($this->getList()));
        $this->set('editId');
    }

    public function edit($id, $page = 1)
    {
        $entity = $this->Tasks->findById($id)->firstOrFail();

        if (!$this->checkAccess($entity)) {
            $this->Flash->error(__('You do not have permission.'));
            return $this->redirect(['action' => 'index']);
        }

        $this->set('listTasks', $this->paginate($this->getList(), ['page' => $page]));

        if ($this->request->is(['post', 'put'])) {
            $this->Tasks->patchEntity($entity, $this->request->getData());
            if ($this->Tasks->save($entity)) {
                $this->Flash->success(__('Your task has been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Unable to update your task.'));
        }

        $this->set('editId', $id);

        return $this->render('index');
    }

    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);
        $entity = $this->Tasks->findById($id)->firstOrFail();

        if (!$this->checkAccess($entity)) {
            $this->Flash->error(__('You do not have permission.'));
            return $this->redirect(['action' => 'index']);
        }

        if ($this->Tasks->delete($entity)) {
            $this->Flash->success(__('The `{0}` article has been deleted.', $entity->name));

            return $this->redirect(['action' => 'index']);
        }
    }

    protected function getList()
    {
        $userId = $this->Authentication->getIdentityData('id');
        $options = $this->isAdmin ? [] : ['conditions' => ['author_id' => $userId]];

        return $this->Tasks->find('all', $options);
    }

    protected function isAuthor($task)
    {
        return $task->author_id === $this->Authentication->getIdentityData('id');
    }

    protected function checkAccess($task)
    {
        return $this->isAdmin || $this->isAuthor($task);
    }
}
