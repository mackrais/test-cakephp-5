<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <NameCategory>
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright
 * @see       <https://mackrais.com>
 * @date      15.12.23
 */

declare(strict_types=1);
/** @var $listTasks */
/** @var $editId */
/** @var $userId */
/** @var App\View\AppView $this */
$hasTasks = count($listTasks) > 0;
?>
<div class=" container rounded-3 my-5 bg-white shadow p-5" style="height:auto;">
    <div>
        <?= $this->Form->create() ?>
        <h1 class="h1"><?= __('Task list') ?></h1>
        <div class="row">
            <div class=" col-8">
                <?= $this->Form->control(
                    'name',
                    [
                        'required' => true,
                        'class' => 'py-3 form-control shadow',
                        'placeholder' => __('Enter your task ...'),
                        'label' => false,
                    ]
                ) ?>
            </div>
            <div class="col-2">
                <?= $this->Form->submit(__('Add'), ['class' => 'mt-2 btn btn-dark']); ?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
    <hr>
    <div class="row rounded bg-white">
        <div class=" col-12">
            <ul class=" list-group" id="list">

                <?php
                if (!$hasTasks) : ?>
                    <h3><?= __('Empty') ?></h3>
                <?php
                endif; ?>

                <?php
                foreach ($listTasks as $task) : ?>
                    <?php
                    $edited = $editId === $task->id ?>
                    <li class=" my-3 py-3 shadow list-group-item ">
                        <div class="row">
                            <div class="col-8">
                                <span class="h4"><?= $task->name ?></span>
                            </div>
                            <div class="col-4">
                                <?= !$edited ? $this->Html->link(
                                    __("Edit"),
                                    ['action' => 'edit', $task->id, $this->request->getQuery('page', 1)],
                                    ['class' => 'me-2 btn btn-dark']
                                ) : '' ?>
                                <?= $this->Form->postLink(
                                    __('Delete'),
                                    ['action' => 'delete', $task->id],
                                    [
                                        'class' => 'btn btn-dark',
                                        'confirm' => __(
                                            'Are you sure you want to delete `{name}` ?',
                                            ['name' => $task->name]
                                        ),
                                    ]
                                )
                                ?>
                            </div>
                        </div>

                        <?php
                        if ($edited): ?>
                            <?= $this->Form->create($task) ?>
                            <div class="row">
                                <div class="col-8">

                                    <?= $this->Form->control(
                                        'name',
                                        [
                                            'required' => true,
                                            'class' => 'py-3 form-control shadow',
                                            'placeholder' => __('Enter your task ...'),
                                            'label' => false,
                                        ]
                                    ) ?>
                                </div>
                                <div class="col-2">
                                    <input type="submit" class="mt-2 me-2 btn btn-success" value="<?= __('Save') ?>">
                                    <?= $this->Html->link(
                                        __("Cancel"),
                                        ['action' => 'index', $this->request->getQuery('page', 1)],
                                        ['class' => 'mt-2 btn btn-danger']
                                    ) ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>

                        <?php endif; ?>

                        <div class="row">
                            <div class="col-6 figure-caption">
                                <small>
                                    <?= __('Created At: ') ?>
                                    <?= $task->created->format(DATE_RFC850) ?>
                                </small>
                                <br>
                                <small>
                                    <?= __('Last modified: ') ?>
                                    <?= $task->created->format(DATE_RFC850) ?>
                                </small>
                            </div>
                            <?php
                            if ($userId !== $task->author_id): ?>
                                <div class="col-6 text-end">
                                    <span class="badge bg-danger"><?= __('You not author') ?></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </li>
                <?php
                endforeach; ?>
            </ul>

            <?php
            if ($hasTasks): ?>
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php
                        echo $this->Paginator->first();
                        echo $this->Paginator->prev();
                        echo $this->Paginator->numbers();
                        echo $this->Paginator->next();
                        echo $this->Paginator->last();
                        ?>
                    </ul>
                </nav>
            <?php
            endif; ?>

        </div>
    </div>
</div>


