<?php
/**
 * Created by PhpStorm.
 * PHP Version: 8.2.
 *
 * @category   <NameCategory>
 *
 * @author     Oleh Boiko <developer@mackrais.com>
 * @copyright  2014-2023 @MackrRais
 *
 * @see       <https://mackrais.com>
 * @date      15.12.23
 */

declare(strict_types=1);

/** @var $user */
?>
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <h2 class="text-center text-dark mt-5"><?= __('Create an account') ?></h2>
            <div class="card my-5">
                <?= $this->Form->create($user, ['class' => 'card-body cardbody-color p-lg-5']) ?>
                <div class="text-center">
                    <img src="https://cdn.pixabay.com/photo/2016/03/31/19/56/avatar-1295397__340.png"
                         class="img-fluid profile-image-pic img-thumbnail rounded-circle my-3"
                         width="200px" alt="profile">
                </div>
                <fieldset>
                    <legend><?= __('Please enter your email and password') ?></legend>
                    <?= $this->Form->control('email', ['required' => true]) ?>
                    <?= $this->Form->control('password', ['required' => true]) ?>
                </fieldset>
                <?= $this->Form->submit(__('Create'), ['class' => 'btn btn-color px-5 mb-5 w-100']); ?>
                <div id="emailHelp" class="form-text text-center mb-5 text-dark"> <?= __('Or you already have an account:') ?>
                    <?= $this->Html->link("Login", ['action' => 'login'], ['class' => 'text-dark fw-bold']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

