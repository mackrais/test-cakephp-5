# CakePHP Application Skeleton

![Build Status](https://github.com/cakephp/app/actions/workflows/ci.yml/badge.svg?branch=master)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)
[![PHPStan](https://img.shields.io/badge/PHPStan-level%207-brightgreen.svg?style=flat-square)](https://github.com/phpstan/phpstan)

A skeleton for creating applications with [CakePHP](https://cakephp.org) 5.x.

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

### Description task [HERE](TASK.md)

## Run project

```shell
docker-compose -f docker-compose.yaml --env-file docker.env up --remove-orphans
```

### Composer install

```shell
docker exec -i test-cakephp-php composer install
```

### Configuration DB connection for Docker
config/app_local.php
```php
   'Datasources' => [
        'default' => [
            'host' => 'db',
            'username' => 'root',
            'password' => 'root',
            'database' => 'test-db-cakephp',
            'url' => env('DATABASE_URL', null),
        ],
]
```

### Run migration

```shell
docker exec -i test-cakephp-php bash /var/www/html/bin/cake migrations migrate
```

### If you want use virtual hosts:

Need add next config to `/etc/hosts`

```
    127.0.0.1 test.loclahost
```

After that, the project will be available at the following links:

[http://127.0.0.1](http://127.0.0.1)

[http://test.loclahost](http://test.loclahost)

[https://test.loclahost](https://test.loclahost)

### Admin credentials

```text
Email: admin@test.localhost
Password: admin
```

### Screenshot
![Screenshot1](https://archive.org/download/screenshot-task-manager-cakephp/Screenshot-task-manager-cakephp.png)
![Screenshot2](https://archive.org/download/screenshot-task-manager-cakephp-1/Screenshot-task-manager-cakephp-1.png)

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

If Composer is installed globally, run

```bash
composer create-project --prefer-dist cakephp/app
```

In case you want to use a custom app dir name (e.g. `/myapp/`):

```bash
composer create-project --prefer-dist cakephp/app myapp
```

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Update

Since this skeleton is a starting point for your application and various files
would have been modified as per your needs, there isn't a way to provide
automated upgrades, so you have to do any updates manually.

## Configuration

Read and edit the environment specific `config/app_local.php` and set up the
`'Datasources'` and any other configuration relevant for your application.
Other environment agnostic settings can be changed in `config/app.php`.

## Layout

The app skeleton uses [Milligram](https://milligram.io/) (v1.3) minimalist CSS
framework by default. You can, however, replace it with any other library or
custom styles.
