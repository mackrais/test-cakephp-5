**Task: Building a Simple Task Management Application**

**Background:**

You are tasked with building a simple web application for managing tasks
using Cake PHP. The application should allow users to create, read,
update, and delete tasks. You should follow best practices, write clean
code, and utilize appropriate architectural patterns.

**Requirements:**

1.  **Backend:**

    1.  Create a Cake PHP project.

    2.  Implement a RESTful API for tasks, including endpoints for:

        1.  Creating a new task.

        2.  Retrieving a list of tasks.

        3.  Updating a task.

        4.  Deleting a task.

    3.  Use CakePHP models for database interactions.

    4.  Implement validation for task creation and updates.

2.  **Bootstrap Frontend:**

    1.  Create a user interface using Bootstrap for the CakePHP
        > application.

    2.  Build a user interface with the following components:

        1.  Task list: Display a list of tasks with the ability to mark
            > tasks as completed and delete them.

        2.  Task creation: Allow users to add new tasks with a title and
            > description.

        3.  Task editing: Implement an edit feature for updating task
            > details.

3.  **Clean Code and Good Code Structure**:

    1.  Follow CakePHP best practices and coding standards.

4.  **Authentication (Bonus):**

    1.  Implement user authentication.

    2.  Ensure that only authenticated users can create, edit, and
        > delete tasks.

5.  **Use version control (e.g., Git) to track your changes.**

**Evaluation Criteria:**

**The task will be evaluated based on:**

1.  Correctness of the functionality.

2.  Code quality, including clean and well-structured code.

3.  Proper use of built-in features.

4.  Adherence to architectural patterns.

5.  Handling of edge cases and errors.

6.  User-friendly interface.

7.  Documentation and instructions.
