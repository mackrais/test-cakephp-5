<?php
declare(strict_types=1);

use App\Model\Table\UsersTable;
use Cake\ORM\Locator\LocatorAwareTrait;
use Migrations\AbstractMigration;

class CreateAdmin extends AbstractMigration
{
    use LocatorAwareTrait;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change(): void
    {
        $table = $this->getTableLocator()->get('Users');
        $user = $table->newEmptyEntity();

        $user->role = UsersTable::ROLE_ADMIN;
        $user->email = 'admin@test.localhost';
        $user->password = 'admin';
        if ($table->save($user)) {
            $this->getOutput()->writeln('Success crated admin user');
        }
    }
}
